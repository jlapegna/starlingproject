package
{
	import flash.display.Sprite;
	
	import net.hires.debug.Stats;
	
	import starling.core.Starling;
	
	[SWF(frameRate="60", width="800", height="600", backgroundColor="0x333333")]
	public class StarlingProject extends Sprite
	{
		private var _stats:Stats;
		private var _myStarling:Starling;
		
		public function StarlingProject()
		{
			_stats = new Stats();
			this.addChild(_stats);
			
			_myStarling = new Starling(Game, stage);
			_myStarling.antiAliasing = 1;
			_myStarling.start();
		}
	}
}